///
/// @package phosim
/// @file phosimcatgen.cpp
/// @brief Generates phosim Object catalog and files with 
///        random or grid of stars and/or galaxies (galaxies not 
///        implimented yet)
///
/// @brief Created by:
/// @author Glenn Sembroski (Purdue)
///
/// @brief Modified by:
/// @author 
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

//   This program uses:
//   The Lean Mean C++ Option Parser. 
//   Written 2012 by Matthias S. Benkmann
//   http://optionparser.sourceforge.net
//   No license needed.

#include <iostream>
#include <fstream>
#include <math.h>
#include <string>
#include <vector>
#include <stdlib.h>
#include <string>
#include <iomanip>
#include <algorithm>    // std::min_element, std::max_element
#include <sstream>

#include "galaxycomplex.h"
#include "../source/ancillary/readtext.h"
#include "basic_types.h"
#include "rng_mwc.h"
using namespace RandomNumbers;

double eraAnp(double a);

void palDtp2s ( double xi, double eta, double raz, double decz,
                double *ra, double *dec );
void palDh2e ( double az, double el, double phi, double *ha, double *dec);

const double  ERFA_D2PI = 6.283185307179586476925287;
const double PAL__DPI = 3.1415926535897932384626433832795028841971693993751;
const int kPrintFreq = 10000;

// const double kBulgeToTotalFlux      = .5541;
// const double kBulgeToTotalFluxSigma = .0198;


// **************************************************************************
class  CGParams
{
public:
  CGParams(int argc, char* argv[]);
  void SetDefaults();
  void PrintOptionValues();
  bool makeStars(){return fMakeStars;};
  bool makeStarGrid(){return fMakeStarGrid;};
  bool makeGalaxies(){return fMakeGalaxies;};
  void LoadMag_DistData(bool ForStars);

  std::string fObservationID;
  uint32 fRandomSeed;
  double fRADegOrigin;
  double fDecDegOrigin;


  //command arguments;
  double fStarsMinimumMagnitude;
  double fStarsMaximumMagnitude;
  double fStarsDiameterFOVDeg;

  double fStarGridSpacingDeg;
  double fStarGridMagnitude;
  double fStarGridWidthDeg;

  double fGalaxiesMinimumMagnitude;
  double fGalaxiesMaximumMagnitude;
  double fGalaxiesDiameterFOVDeg;


private:
  bool fMakeStars;
  bool fMakeStarGrid;
  bool fMakeGalaxies;

public:
  int           fCatalogObjectID;
  int           fCatalogObjectCount;
  double        fStarDensityPerDeg2;
  double        fGalaxyDensityPerDeg2;
  std::string   fStarGridSEDFileName;
  std::string   fCatFileName;
  std::ofstream fCatFile;
  std::string   fDataDir;
  //double fMinimumMagnitude;
  //double fMaximumMagnitude;

  std::vector< double > fStarMagDensity;
  std::vector< double > fGalaxyMagDensity;

  std::vector< double > fStarMagCumulativeDensity;
  std::vector< double > fGalaxyMagCumulativeDensity;
  std::vector< double > fStarMagCumulativeProb;
  std::vector< double > fGalaxyMagCumulativeProb;

};
// ***********************************************************************
  
CGParams::CGParams(int argc, char* argv[])
{
  // *****************************************************************
  // This code has little to no error checking. Assumed this program is run 
  // by another program (phosim.py usually)  which knows how things should
  // be setup
  // *****************************************************************

  SetDefaults();

  // This constructer parses the parametersa form the input (obs_*.pars) 
  // file comming in though stdin.

  std::string cmdArguments;

  // Read parameters from stdin.
  readText pars(std::cin);
  for (size_t t(0); t < pars.getSize(); t++) {
	std::string line(pars[t]);
	readText::get(line, "obshistid",   fObservationID);
	readText::get(line, "obsseed",     fRandomSeed);
	readText::get(line, "pointingra",  fRADegOrigin);
	readText::get(line, "pointingdec", fDecDegOrigin);
	readText::get(line, "datadir",     fDataDir);  //needed for finding 
                                                   //mag_dist file

	std::istringstream iss(line);
	std::string keyName;
	iss >> keyName;

	if ( keyName == "stars") {
	  fMakeStars = true;
	  iss >> fStarsMinimumMagnitude >> fStarsMaximumMagnitude 
		  >> fStarsDiameterFOVDeg;
	}

  	if ( keyName == "stargrid") {
	  fMakeStarGrid = true;
	  iss >> fStarGridSpacingDeg >> fStarGridWidthDeg >> fStarGridMagnitude;
	}

  	if ( keyName == "galaxies") {
	  fMakeGalaxies = true;
	  iss >> fGalaxiesMinimumMagnitude >> fGalaxiesMaximumMagnitude 
		  >> fGalaxiesDiameterFOVDeg;
	}
  }
 }
// **********************************************************************

void CGParams::PrintOptionValues()
{
  std::cout<< " obervation ID: " << fObservationID << std::endl;
  std::cout<< " ra(deg): " << fRADegOrigin << std::endl;
  std::cout<< " dec(deg):  "<< fDecDegOrigin << std::endl;
  std::cout<< " random number generator seed: " << fRandomSeed << std::endl;
  if (fMakeStars) {
	std::cout << " make stars: True"      << std::endl;
	std::cout << "  min stars magnitude: " << fStarsMinimumMagnitude 
			  << std::endl;
	std::cout << "  max stars magnitude: " << fStarsMaximumMagnitude 
			  << std::endl;
	std::cout << "  stars FOV Diameter(deg): " << fStarsDiameterFOVDeg 
			  << std::endl;
  }
  else{
	std::cout << " make stars: False" << std::endl;
  }

  if (fMakeStarGrid) {
	std::cout<< " make stargrid: True" << std::endl;
	std::cout<< "  stargrid Spacing(deg): " << fStarGridSpacingDeg 
			 << std::endl;
	std::cout<< "  stargrid Magnitude(deg): " << fStarGridMagnitude 
			 << std::endl;
	std::cout<< "  stargrid FOV Diameter(deg): " << fStarGridWidthDeg 
			 << std::endl;
  }
  else{
	std::cout<< " make stargrid: False"<< std::endl;
  }
  
  if (fMakeGalaxies) {
	std::cout<< " make galaxies: True" << std::endl;
	std::cout<< "  min galaxies magnitude: " << fGalaxiesMinimumMagnitude 
			 << std::endl;
	std::cout<< "  max galaxies magnitude: " << fGalaxiesMaximumMagnitude 
			 << std::endl;
	std::cout<< "  galaxies FOV Diameter(deg): " << fGalaxiesDiameterFOVDeg 
			 << std::endl;
  }
  else{
	std::cout<< " make galaxies: False" << std::endl;
  }
  
  std::cout << "-----------------------------------------------------------"
	"-------------------------------" << std::endl;
  return;
}
// **********************************************************************

void CGParams::SetDefaults()
{
  fMakeStars    = false;
  fMakeStarGrid = false;
  fMakeGalaxies = false;
  fStarGridSEDFileName = "../sky/sed_flat.txt";
  return;
}
// ************************************************************************

void CGParams::LoadMag_DistData(bool ForStars)
{
  // ***************************************************************
  // If its not "ForStars=true" then its ForGalaxies!
  // Read in the magnitude table, colums are:
  // Magnitude:number of stars per sq. degree:number of galaxy per sq deg
  //  A copy of the "mag_dist" table is in the phosim/data directory. I 
  // don't think it is what we finally want but it will work as a 
  // place holder for now.
  // ****************************************************************
 
  bool fDebugPrint=false;

  // *********************************
  // Read in the mag_dist file: Read in both but only use 1. (star or galaxy)
  // *********************************
  int mm;
  double starDensity;
  fStarMagDensity.clear();

  double galaxyDensity;
  fGalaxyMagDensity.clear();

  std::ifstream mag;
  std::string magDistFilePath= fDataDir + "/../validation/mag_dist";
  mag.open(magDistFilePath.c_str());

  if ( ! mag.is_open() ) { 
    std::cout<<"Fatal- Can not find local star/galaxy  magnitude file:"
			 << magDistFilePath << std::endl;
    exit(1);
  }

  while( ! mag.eof() ) {
    mag >> mm >> starDensity >> galaxyDensity;
	if(fDebugPrint) {
	  std::cout << mm << " " << starDensity << " " <<  galaxyDensity 
				<<std::endl;
	}

    fStarMagDensity.push_back( starDensity);
    fGalaxyMagDensity.push_back( galaxyDensity);
  }

  // **************************************************************
  // Set up to Determine cumulative density and probability
  // *****************************************
  // Starting at the bin that contains the minimum required M  start 
  // forming the cumulative density (which will be converted into a 
  // cumulative probability) to the bin containing  max
  // **************************************************************
  std::vector < double >* pMagDensity;
  std::vector < double >* pMagCumulativeDensity;
  std::vector < double >* pMagCumulativeProb;
  int minM=0;
  int maxM=30;
  double* pDensityPerDeg2;

  if ( ForStars) {
	
	pMagDensity =  &fStarMagDensity;
	pMagCumulativeDensity =  &fStarMagCumulativeDensity;
	pMagCumulativeProb =  &fStarMagCumulativeProb;
	minM=(int)fStarsMinimumMagnitude;
	maxM=(int)fStarsMaximumMagnitude;
	if (maxM >= fStarsMaximumMagnitude) {
	  maxM=maxM-1;
	}
	fStarDensityPerDeg2 = 0;
	pDensityPerDeg2 = &fStarDensityPerDeg2;
  }
  else {    //For galaxies
	pMagDensity =  &fGalaxyMagDensity;
	pMagCumulativeDensity =  &fGalaxyMagCumulativeDensity;
	pMagCumulativeProb =  &fGalaxyMagCumulativeProb;
	minM=(int)fGalaxiesMinimumMagnitude;
	maxM=(int)fGalaxiesMaximumMagnitude;
	if (maxM >= fGalaxiesMaximumMagnitude) {
	  maxM=maxM-1;
	}
	fGalaxyDensityPerDeg2 = 0;
	pDensityPerDeg2 = &fGalaxyDensityPerDeg2;
  }
  


  // ***********************************************
  // Assume here that the mag_dist table spans our requested magnitude range
  // Note: min and max M may be different for stars and galaxies
  // ***********************************************
  double magDensitySum=0.0;

  // ******************************************************************
  // Dertermine (seperatly for clarity):
  // 1: total density from minM to maxM: To use for probablility normilization.
  for (int i = minM; i <= maxM; i++) {   //includes maxM bin
    magDensitySum += pMagDensity->at( i );
  }
  *pDensityPerDeg2 = magDensitySum;

  
  // ***********************
  // 2: Cumulative Density
  // ***********************
  pMagCumulativeDensity->clear();
  pMagCumulativeDensity->resize( fStarMagDensity.size(),0);
  for (int i = minM; i <= maxM; i++) {   //includes maxM bin
	// **************************
	// This is a little triky but works.
	// **************************
	if (i != 0){
      pMagCumulativeDensity->at(i) =  pMagDensity->at(i) + 
	                                        pMagCumulativeDensity->at(i-1);
    }
	else {
	  pMagCumulativeDensity->at(i) =  pMagDensity->at(i);
	}
  }

  // ***************************
  // 3: Normalize cumulative density to a cumulative probablity
  // ***************************
  pMagCumulativeProb->clear();
  pMagCumulativeProb->resize( fStarMagDensity.size(),0);
  for (int i = minM; i <= maxM; i++) {
	pMagCumulativeProb->at(i) = pMagCumulativeDensity->at(i) / magDensitySum;
  }
}

// ******************************************************************

/*
int CGParams::GetLastObjectID(std::ifstream& inCatFile)
// **************************************************************************
// Search the inCatFile for the last line which starts with the string Object
// Second string in that line will be the last object ID. Return the integer 
// part of that. (Object id format example:   187689.1 or 187689.2
// **************************************************************************
{
  // **********************************************************************
  // Do this the dumb slow stupid way that is guaranteed to work. Just read 
  // in the whole file line by line until we reach the eof. Return the 
  // interger part of the ObjectID of the last object read in.
  // **********************************************************************
  std::string catLine;
  double catLastObjectID;
  std::istringstream catIss;
  std::string catObj;
  double catFullObjectID;

  catLastObjectID = 0;

  while (getline (inCatFile,catLine))
	{
	  if ( inCatFile.eof() ) {
		return catLastObjectID;
	  }
	  else{
		catIss.str(catLine);
		catIss >> catObj;
		if (catObj == "object") {   //test this line is good
		  catIss >> catFullObjectID; 
		}
	  }
	}
  return int(catFullObjectID);
}
*/
// **************************************************************************

class CatalogObject
{
public:
  CatalogObject(CGParams* pCGPar );
  void   GenerateSingleRandomRaDec();
  void   GenerateSingleRandomMagnitude( bool ForStars);
  void   GenerateGridRaDec(double RARad,  double DecRad, 
				  double FOVDeg, double StepSizeDeg);
  double GetRADeg(){return fRARad*180.0/PAL__DPI;};
  double GetDecDeg(){return fDecRad*180.0/PAL__DPI;};
  void   SetMagnitude(double mag ){fMagnitude=mag;return;};
  double GetMagnitude(){return fMagnitude;};
  void   SetStarsSEDFileName()
              {fSEDFileName=pCGParams->fStarGridSEDFileName;return;};
  void   SetStarGridSEDFileName()
              {fSEDFileName=pCGParams->fStarGridSEDFileName;return;};
  void   SetGalaxySEDFileName()
              {fSEDFileName=pCGParams->fStarGridSEDFileName;return;};
  std::string GetSEDFileName(){return fSEDFileName;};

  bool        fDebugPrint;

protected:
  CGParams* pCGParams;

  std::vector < double > fRightAscension;
  std::vector < double > fDeclination;

  std::vector < double > fStarMagDensity;
  std::vector < double > fGalaxyMagDensity;

  std::vector < double > fStarMagCumulativeDensity;
  std::vector < double > fGalaxyMagCumulativeDensity;
  std::vector < double > fStarMagCumulativeProb;
  std::vector < double > fGalaxyMagCumulativeProb;

  //Object vaiables
  double fRARad;
  double fDecRad;
  double fMagnitude;
  double fZenithMaxRange;
  std::string fSEDFileName;
  std::string fStarSEDFileName;
  std::string fStarGridSEDFileName;
  std::string fGalaxySEDFileName;

}; 
// ***************************************************************************

CatalogObject::CatalogObject(CGParams* pCGPar)
{
  pCGParams =  pCGPar;
}
// ********************************************************************** 

void CatalogObject::GenerateSingleRandomRaDec()
{
  // ****************************************************************
  // We have to be a little clever in generating the random 
  // palcement. 
  // From:   // http://mathworld.wolfram.com/SpherePointPicking.html
  // 1: Pick randomly in an alt/az over a spherical cap centered at
  //  zenith. alt=90,az=0,0.
  // 2:Use the conversion program to convert the az/el to the 
  // correct ra/dec
  // ****************************************************************
  // Gen random az,el in cap with fZenithMaxRange. Set in Constructor
  // ***************************************
  double azimuthRad=RngDouble() * 2 * PAL__DPI;  //Uniform in azimuth
                                          //zenith 0 to zenithMaxRange in cap
  double zenithRad=acos( 1. - 2. * RngDouble() * fZenithMaxRange);
  double elevationRad = PAL__DPI / 2. - zenithRad;
  
  double longRad = pCGParams->fRADegOrigin * PAL__DPI / 180.0;
  double latRad  = pCGParams->fDecDegOrigin * PAL__DPI / 180.0;
  double haRad;
 
  // *********************************
  // Use generic conversion function from Slalib (From pallib actually for 
  // licensing issues)
  // ******************************** 

  palDh2e(azimuthRad,elevationRad ,latRad ,&haRad, &fDecRad );
  fRARad=longRad-haRad;
  if (fRARad<0.0) {
	fRARad= 2.0*PAL__DPI+fRARad;
  }
  return;
}
// ****************************************************************

void CatalogObject::GenerateSingleRandomMagnitude( bool ForStars)
// *****************************************************************
// Search forward  through the cumulative table and then interpolate
// *****************************************************************
{
  // *******************************************
  // Setup for Star OR Galaxies
  // *******************************************
  std::vector < double >* pMagCumulativeProb;
  double minimumMagnitude;
  double maximumMagnitude;

  if (ForStars) {
	pMagCumulativeProb =  &pCGParams->fStarMagCumulativeProb;
	minimumMagnitude=pCGParams->fStarsMinimumMagnitude;
	maximumMagnitude=pCGParams->fStarsMaximumMagnitude;
  }
  else {                  // its for Galaxies
	pMagCumulativeProb =  &pCGParams->fGalaxyMagCumulativeProb;
	minimumMagnitude=pCGParams->fGalaxiesMinimumMagnitude;
	maximumMagnitude=pCGParams->fGalaxiesMaximumMagnitude;
  }


  int minM = (int)minimumMagnitude;   // minM is at or  below actual limit
  int maxM = (int)maximumMagnitude; 
  if( maxM >= maximumMagnitude) {
	maxM=maxM-1;                       //maxM  is below upper limit
  }

  // ***************************************************************
  // Pick a probability and find which M bin it is in.
  // ***************************************************************
  double r = RngDouble();
 
  // ***********************
  // This r will define a M bin for us in our range minM to maxM. Find the M
  // ***********************
  int M=30;
  for( int i = minM; i <= maxM; i++) {
	if (r <= pMagCumulativeProb->at( i ) ) {
	  M=i;
	  break;
	}
  }

  // This gets us the M bin that contain the r probabil;ity.
  // We could assume that the cumulative probability changes  
  // linerly between magnitudes.But we don't! Maybe later 
  
  // **************************************************
  // 3 possiblities:
  // 1: i is in some other M bin. (in the middle of range)
  // 2: i=minM but MiniimumMagnitude>minM (our minimimMagnitude is in
  //     middle of M bin (example 13.75)
  // 3: i=MaxM but MaxmimumMagnitude>maxM (our maximumMagnitude is in
  //     middle of M bin (example 27.6 , or even i=26 and 
  //     maximumMagnitude=27.0)
  // *****************************************************
  
  if ( M > minM && M < maxM) {      
	fMagnitude = (double)(M) +  RngDouble();
  }
  else if( M == minM) {
	fMagnitude = minimumMagnitude +RngDouble()*(minM+1- minimumMagnitude);
  }
  else if ( M == maxM ) {
	fMagnitude = maxM+RngDouble()*(maximumMagnitude-maxM);
  }
  else{
	std::cout << "GenerateSingleRandomMagnitude--Fatal: r and M out of range"
			<<std::endl;
	exit(1);
	// The above should never happen.
  }
  return;
}
// *****************************************************************

void CatalogObject::GenerateGridRaDec(double RARad,  double DecRad, 
									  double FOVDeg, double StepSizeDeg)
// *****************************************************************
// Generates a square grid of focalplane locations(in deg) and 
// converts to equivalent ( tangent plane or gnomonic) projection to
// the celestial sphere in ra/dec centered at the specified ra/dec
// *****************************************************************
// Signs of directions will be determined later. Just go with 
// "whatever" for now.
// ****************************************************************
// Make up a vector of vectors grid of focal plane locations. 
// Outermost is "Y" which is along altitude coord.
// Results in Catalog members: "rightAscension" and "declination" vectors.
// ****************************************************************
{
  bool debugPrint = false;
  fRightAscension.clear();
  fDeclination.clear();


  // ****************************************************************
  // Determin xPintDeg,yPointDeg  (in focal plane);
  // *******************************
  
  int npoints =  FOVDeg / StepSizeDeg;

  // Since we want a point at the very center this should be odd
  if (npoints%2 == 0) {
    npoints=npoints+1;
  }

  std::cout << " #Star Grid spacing(deg):" << StepSizeDeg 
			<< std::endl;
  std::cout << " #Number of points across grid: " << npoints << std::endl;
  std::cout << " #Number of points in grid:     " << npoints*npoints 
			<< std::endl;
  
  if (npoints*npoints > kPrintFreq) {
	std::cout << " This may take a few minutes. Be patinet!" << std::endl;
	std::cout <<" Grid Ra/Dec creation progress: each # = " << kPrintFreq 
			  <<" grid points completed" << std::endl;
	std::cout<< " " << std::flush; //Start off a little offset to lineup with
	                               // above 
  }

  double halfWidth=( ( npoints - 1 ) /2 ) * StepSizeDeg;

  for ( int j = 0; j < npoints; j++ ) {

    double yPointDeg= (- halfWidth) + j * StepSizeDeg;

    for ( int i = 0 ;i < npoints ; i++ ) {

      double xPointDeg= (- halfWidth) + i * StepSizeDeg;

 	  if (debugPrint) {
		std::cout << "xPointDeg, yPointDeg: "<<  xPointDeg << " " << yPointDeg
				  << std::endl;
	  }

	  // *******************************************************
	  // Project xPointDeg and yPointDeg back to celestial sphere and 
	  // get Ra/dec for this grid point.
	  // ********************************************************
      double raGridRad;
      double decGridRad;

	  // **********************************************************
	  // Use palDtp2s. (Tangent Plane to Spherical)  gnomonic projection
	  // function which we coppied to here. (Allowed under pallib/erfam
	  // license)
	  // **********************************************************

	  palDtp2s(xPointDeg * PAL__DPI / 180., yPointDeg *  PAL__DPI / 180., 
			   RARad,  DecRad, &raGridRad,  &decGridRad);

      // *****************************
	  // Save the ra/dec of the grid point.
	  // ****************************
	  fRightAscension.push_back(raGridRad);
      fDeclination.push_back(decGridRad);
 
	  if (debugPrint) {
		std::cout<<xPointDeg<< " " <<  yPointDeg << " " << raGridRad << " "
			   << decGridRad<<std::endl;
		std::cout << "at point: " <<  fRightAscension.size() 
				  << ": raGridRad,decGridRad: "<<  raGridRad << " " 
				  << decGridRad << std::endl;
	  }

	  if ( fRightAscension.size()%kPrintFreq == 0) {
		std::cout<< "#" << std::flush;
	    if  ( fRightAscension.size()%(kPrintFreq*10) == 0) {
		  std::cout<< "|" << std::flush;
		}
	  }
    }
  }
  return;
}
// ****************************************************************

class StarObject :public CatalogObject
// **********************************************************
// Note this class derived from CatalogObject and thus has 
// CatalogObject's methods
// **************************************************
{
 public:
  StarObject(CGParams* pCGPar, std::ofstream& objFile);
  void GenerateRandomStars();
  void GenerateGridStars();
  void GenerateRaDecFromStarXYMFile(std::string fileName);
 
private:
  //CGParams* pCGParams;
  std::ofstream* ofs;
};
// *****************************************************************


StarObject::StarObject(CGParams* pCGPar, std::ofstream& objFile):
                        CatalogObject(pCGPar)  
{
  //pCGParams = pCGPar;
 
  // ***************************
  // So at this point we have:
  //  pCGParams->fStarMagDensity.at(i)  :Density of stars from i=M to M+1/deg2
  //  pCGParams->fStarMagCumulativeDensity  :Cumulative Density for stars from
  //                                       M=0(start of mag_dist table) to M=i
  //  pCGParams->fStarMagCumulativeProb.at(i) :Cumulative probaility for 
  //                           stars from  M=0(start of mag_dist table) to M=i
  //  pCGParams->starDensityPerDeg2: Density of stars in our Magnitude range.
  // Same for galaxies
  // ***************************  

  //fZenithMaxRange used to generate  ra/dec
  fZenithMaxRange = ( 1. - cos( ( pCGParams->fStarsDiameterFOVDeg / 2. )
							   * PAL__DPI / 180.0 ) ) / 2.0;

  //pCGParams->SetMinimumMagnitude(pCGParams->fStarsMinimumMagnitude);
  //pCGParams->SetMaximumMagnitude(pCGParams->fStarsMaximumMagnitude);

  ofs = &objFile;
}

void StarObject::GenerateRandomStars()
// *****************************************************************
// Generate star objects randomly over our fov and append them to
// the object file. Be careful with the random placement so that the local 
// density at all points in the FOV is the same.  Use the file 'mag_dist' to 
// get the distribution of star magnitudes(in CatalogObject constructor). Use 
// flat SED. (We need to  improve upon this with a good color 
// Magnitude/galactic latude function from some star count program)
// ******************************************************************
{
  bool forStars = true;
  pCGParams->LoadMag_DistData( forStars );

  // *************************** 
  // Now make the objects
  // ***************************
  // Number of stars to make from density times cap area
  double areaFOVsr = ( 1. - cos( ( pCGParams->fStarsDiameterFOVDeg / 2. )
								 * PAL__DPI / 180.0 ) );

  double areaFOVDeg2=areaFOVsr * (180/ PAL__DPI) * (180/ PAL__DPI);
  int numStars = pCGParams->fStarDensityPerDeg2 * areaFOVDeg2;
  std::cout<< "Making "<< numStars << " Random Star Objects " << std::endl;

  SetStarsSEDFileName();//This just flat for now until we find a better way.

  // **************************
  //Here we go!
  // **************************
  for (int i=0; i<numStars; i++) {
    GenerateSingleRandomRaDec(); // Generates a random alt/az and converts to 
	                             // the Ra/dec such that stars are within our
	                             // user defined field-of-view.

	//Pick from our culumlative distribution 
	GenerateSingleRandomMagnitude( forStars );
								

    pCGParams->fCatalogObjectID++;   //Bump up the object ID

    // Now write the star object to the catalog file
    *ofs<< "object " << pCGParams->fCatalogObjectID << " " << std::fixed 
       << std::setprecision(8) << GetRADeg()<< " " << GetDecDeg() 
       << "  " << std::setprecision(6) << GetMagnitude() << " " 
       << GetSEDFileName() << " 0.0 0.0 0.0 0.0 0.0 0.0 star none  none" 
       << std::endl;
  }
  pCGParams->fCatalogObjectCount += numStars;
  return;
}
// ************************************************************

void StarObject::GenerateGridStars()
{
  // *******************************************************
  //Using a single default (for now) SED for all grid stars
  // *******************************************************
  SetStarGridSEDFileName();              //CatalogObject Method

  // ***************************************************************
  //Get Ra/Dec of where the tel is pointing
  // ***************************************************************
  double raRad = pCGParams->fRADegOrigin * PAL__DPI / 180.0;
  double decRad  = pCGParams->fDecDegOrigin * PAL__DPI / 180.0;


  // **************************************************************
  //Generate the grid locations, centered on raRad,decRad.
  // Places ra/dec values for grid in rightAscension and declination
  // vectors.
  // **************************************************************
  GenerateGridRaDec(raRad, decRad, pCGParams->fStarGridWidthDeg, 
					       pCGParams->fStarGridSpacingDeg);   
                                                 //CatalogObject method
  SetMagnitude(pCGParams->fStarGridMagnitude);
  
  int numRaDecPoint=fRightAscension.size();
  std::cout << "Making " << numRaDecPoint << "Star Grid Objects" << std::endl;

  // ************************************
  // Write star object to catalog file
  // ************************************
  for ( int i = 0; i < numRaDecPoint; i++ ) {
    pCGParams->fCatalogObjectID++;
    *ofs<< "object " << pCGParams->fCatalogObjectID << " " << std::fixed 
       << std::setprecision(8) << fRightAscension.at(i) * 180.0 / PAL__DPI 
       << " " << fDeclination.at(i) * 180.0 / PAL__DPI 
       << "  " << std::setprecision(6) << GetMagnitude()
       << " "  <<  GetSEDFileName() 
       << " 0.0 0.0 0.0 0.0 0.0 0.0 star none none" 
       << std::endl;
  }
  pCGParams->fCatalogObjectCount += numRaDecPoint;
  return;
}
// ****************************************************************

class GalaxyObject :public CatalogObject
// **********************************************************
// Note this class derived from CatalogObject and thus has 
// CatalogObject's methods
// **************************************************
{
 public:
  GalaxyObject(CGParams* pCGPar, std::ofstream& objFile);
  void GenerateComplexGalaxies(double FOVDeg);

private:
  std::ofstream* ofs;
};
// *****************************************************************

GalaxyObject::GalaxyObject(CGParams* pCGPar, std::ofstream& objFile):
                        CatalogObject(pCGPar)  
{
  // ***************************
  // So at this point we have:
  //  pCGParams->fGalaxyMagDensity.at(i) :Density of stars 
  //                                     from i=M to M(i+1)/deg2
  //  pCGParams->fGalaxyMagCumulativeDensity  :Cumulative Density for stars 
  //                                 from M=0(start of mag_dist table) to M=i
  //  pCGParams->fStarMagCumulativeProb.at(i) :Cumulative probaility for 
  //                           stars from  M=0(start of mag_dist table) to M=i
  //  pCGParams->starDensityPerDeg2: Density of stars in our Magnitude range.
  // Same for galaxies
  // ***************************  

  //fZenithMaxRange used to generate  ra/dec
  fZenithMaxRange = ( 1. - cos( ( pCGParams->fGalaxiesDiameterFOVDeg / 2. )
							   * PAL__DPI / 180.0 ) ) / 2.0;
  ofs = &objFile;
}
// **************************************************************************


void GalaxyObject::GenerateComplexGalaxies(double FOVDeg)
{
  // **************************************************************
  // Generate sersicComplex galaxies using idl code form John Peterson 
  // converted to C++
  // **************************************************************

  SetGalaxySEDFileName();//This just flat for now until we find a better way. 

  GalaxyComplex complexGalaxy(FOVDeg);
  int numGalaxies = complexGalaxy.GetNumberOfGalaxies();
  std::cout<< "Making " << numGalaxies <<" Galaxies, "<< numGalaxies 
		   << " Galaxy (Disk and Bulge)" << std::endl;

  GalacticComplexObject disk;
  GalacticComplexObject bulge;
  int numGalaxiesInRange=0;
  for (int i=0; i<numGalaxies; i++) {
    GenerateSingleRandomRaDec(); // Generates a random alt/az and converts to 
	                             // the Ra/dec such that Galaxies are within 
	                             // our user defined field-of-view.

    pCGParams->fCatalogObjectID++;   //Bump up the object ID
 
	
	std::ostringstream osBulgeID;
	osBulgeID << pCGParams->fCatalogObjectID << ".1";
	bulge.ID=osBulgeID.str();
	bulge.RADeg  = GetRADeg();
	bulge.DecDeg = GetDecDeg();
	bulge.SEDFileName = GetSEDFileName();

	std::ostringstream osDiskID;
	osDiskID << pCGParams->fCatalogObjectID << ".2";
	disk.ID = osDiskID.str();
	disk.RADeg   = GetRADeg();
	disk.DecDeg  = GetDecDeg();
	disk.SEDFileName = GetSEDFileName();


	//Determine a random galaxy: z Bulge and disk M and sersciComplex
	//Generate Bulge and disk  objects  
	// Test that it falls within user specified limits and if so
	// write to catalog file, otherwise drop it, but count it towards 
	// number of galaxies we are to create.
	  double MApparent = complexGalaxy.GenerateGalaxy(bulge,disk);
	
	  if ( MApparent >= pCGParams->fGalaxiesMinimumMagnitude &&
		   MApparent <= pCGParams->fGalaxiesMaximumMagnitude) {
		complexGalaxy.WriteObject( bulge, ofs);
		complexGalaxy.WriteObject( disk,  ofs);
		numGalaxiesInRange ++;
	  }
  }


  //count the objects we add to catalog.
  //Each galaxy requirtes 2 objects(disk and bulge)
  std::cout<< "Number of galactic objects (Disk and Bulge) within requested "
	          "magnitude range added to catalog: " << numGalaxiesInRange
		   << std::endl;
  pCGParams->fCatalogObjectCount += 2*numGalaxiesInRange;
  return;
}
// ***********************************************************************


// **************************************************************************
// Following included explicitly here to minimize dependencies.
// ***************************************************************************

/*
**  Copyright (C) 2013-2015, NumFOCUS Foundation.
**  All rights reserved.
**  
**  This library is derived, with permission, from the International
**  Astronomical Union's "Standards of Fundamental Astronomy" library,
**  available from http://www.iausofa.org.
**  
**  The ERFA version is intended to retain identical functionality to
**  the SOFA library, but made distinct through different function and
**  file names, as set out in the SOFA license conditions.  The SOFA
**  original has a role as a reference standard for the IAU and IERS,
**  and consequently redistribution is permitted only in its unaltered
**  state.  The ERFA version is not subject to this restriction and
**  therefore can be included in distributions which do not support the
**  concept of "read only" software.
**  
**  Although the intent is to replicate the SOFA API (other than
**  replacement of prefix names) and results (with the exception of
**  bugs;  any that are discovered will be fixed), SOFA is not
**  responsible for any errors found in this version of the library.
**  
**  If you wish to acknowledge the SOFA heritage, please acknowledge
**  that you are using a library derived from SOFA, rather than SOFA
**  itself.
**  
**  
**  TERMS AND CONDITIONS
**  
**  Redistribution and use in source and binary forms, with or without
**  modification, are permitted provided that the following conditions
**  are met:
**  
**  1 Redistributions of source code must retain the above copyright
**    notice, this list of conditions and the following disclaimer.
**  
**  2 Redistributions in binary form must reproduce the above copyright
**    notice, this list of conditions and the following disclaimer in
**    the documentation and/or other materials provided with the
**    distribution.
**  
**  3 Neither the name of the Standards Of Fundamental Astronomy Board,
**    the International Astronomical Union nor the names of its
**    contributors may be used to endorse or promote products derived
**    from this software without specific prior written permission.
**  
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
**  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE.
**  
*/
//#include "erfa.h"

double eraAnp(double a)
/*
**  - - - - - - -
**   e r a A n p
**  - - - - - - -
**
**  Normalize angle into the range 0 <= a < 2pi.
**
**  Given:
**     a        double     angle (radians)
**
**  Returned (function value):
**              double     angle in range 0-2pi
**
**  Copyright (C) 2013-2015, NumFOCUS Foundation.
**  Derived, with permission, from the SOFA library.  See notes at end of file.
*/
{
   double w;

   w = fmod(a, ERFA_D2PI);
   if (w < 0) w += ERFA_D2PI;

   return w;

}

// **************************************************************************

/*
*+
*  Name:
*     palDh2e

*  Purpose:
*     Horizon to equatorial coordinates: Az,El to HA,Dec

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palDh2e( double az, double el, double phi, double * ha, double * dec );

*  Arguments:
*     az = double (Given)
*        Azimuth (radians)
*     el = double (Given)
*        Elevation (radians)
*     phi = double (Given)
*        Observatory latitude (radians)
*     ha = double * (Returned)
*        Hour angle (radians)
*     dec = double * (Returned)
*        Declination (radians)

*  Description:
*     Convert horizon to equatorial coordinates.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - All the arguments are angles in radians.
*     - The sign convention for azimuth is north zero, east +pi/2.
*     - HA is returned in the range +/-pi.  Declination is returned
*       in the range +/-pi/2.
*     - The latitude is (in principle) geodetic.  In critical
*       applications, corrections for polar motion should be applied.
*     - In some applications it will be important to specify the
*       correct type of elevation in order to produce the required
*       type of HA,Dec.  In particular, it may be important to
*       distinguish between the elevation as affected by refraction,
*       which will yield the "observed" HA,Dec, and the elevation
*       in vacuo, which will yield the "topocentric" HA,Dec.  If the
*       effects of diurnal aberration can be neglected, the
*       topocentric HA,Dec may be used as an approximation to the
*       "apparent" HA,Dec.
*     - No range checking of arguments is done.
*     - In applications which involve many such calculations, rather
*       than calling the present routine it will be more efficient to
*       use inline code, having previously computed fixed terms such
*       as sine and cosine of latitude.

*  History:
*     2012-02-08 (TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1996 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

//#include "pal.h"
#include <math.h>

void
palDh2e ( double az, double el, double phi, double *ha, double *dec) {

  double sa;
  double ca;
  double se;
  double ce;
  double sp;
  double cp;

  double x;
  double y;
  double z;
  double r;

  /*  Useful trig functions */
  sa = sin(az);
  ca = cos(az);
  se = sin(el);
  ce = cos(el);
  sp = sin(phi);
  cp = cos(phi);

  /*  HA,Dec as x,y,z */
  x = -ca * ce * sp + se * cp;
  y = -sa * ce;
  z = ca * ce * cp + se * sp;

  /*  To HA,Dec */
  r = sqrt(x * x + y * y);
  if (r == 0.) {
    *ha = 0.;
  } else {
    *ha = atan2(y, x);
  }
  *dec = atan2(z, r);

  return;
}
// ************************************************************************
/*
*+
*  Name:
*     palDtp2s

*  Purpose:
*     Tangent plane to spherical coordinates

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palDtp2s( double xi, double eta, double raz, double decz,
*               double *ra, double *dec);

*  Arguments:
*     xi = double (Given)
*        First rectangular coordinate on tangent plane (radians)
*     eta = double (Given)
*        Second rectangular coordinate on tangent plane (radians)
*     raz = double (Given)
*        RA spherical coordinate of tangent point (radians)
*     decz = double (Given)
*        Dec spherical coordinate of tangent point (radians)
*     ra = double * (Returned)
*        RA spherical coordinate of point to be projected (radians)
*     dec = double * (Returned)
*        Dec spherical coordinate of point to be projected (radians)

*  Description:
*     Transform tangent plane coordinates into spherical.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-08 (TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

//#include "pal.h"
//#include "pal1sofa.h"

#include <math.h>

void
palDtp2s ( double xi, double eta, double raz, double decz,
           double *ra, double *dec ) {

  double cdecz;
  double denom;
  double sdecz;
  double d;

  sdecz = sin(decz);
  cdecz = cos(decz);
  denom = cdecz - eta * sdecz;
  d = atan2(xi, denom) + raz;
  *ra = eraAnp(d);
  *dec = atan2(sdecz + eta * cdecz, sqrt(xi * xi + denom * denom));

  return;
}
// ***************************************************************************

int main(int argc, char* argv[])
{
  //std::cout << "-----------------------------------------------------------"
  //             "-------------------------------" << std::endl;
  std::cout << "Phosim Catalog Generator" << std::endl;
  std::cout << "-----------------------------------------------------------"
	           "-------------------------------" << std::endl;

  // First handle the input options.
  CGParams* pCGParams = new CGParams(argc,argv);
  pCGParams->PrintOptionValues();

  RngSetSeed32(pCGParams->fRandomSeed);// Need to do this before we use random 
                                       // numbers
  
  // **********************************************************************
  // Initalize object IDs to start at zero. While this may duplicate object 
  // ids in other catalogs thats ok.
  // **********************************************************************
  pCGParams->fCatalogObjectID=0;    
  pCGParams->fCatalogObjectCount=0;    


  // *************************************************************************
  // Now we start to produce objects for the catalog.  We may have a combined 
  // catalog of stars, galaixies and a stargrid as chosen by user.
  // *********************************************************************
  // Set up the catalog file name and open the catalog
  if (pCGParams->makeStars() || pCGParams->makeStarGrid() || 
	  pCGParams->makeGalaxies()) {
	

	pCGParams->fCatFileName = "catgen_" + pCGParams->fObservationID +  ".cat";
	pCGParams->fCatFile.open(pCGParams->fCatFileName.c_str(), std::ios::out);
  }
  else{
	std::cout<<" No catalog request found. No catalog made"<< std::endl;
	pCGParams->fCatFile.close();
	exit(1);
  }

  // *************************************************************************
  // First are random stars
  // *************************************************************************
  if ( pCGParams->makeStars() ) {
	StarObject starObj(pCGParams,pCGParams->fCatFile);
	starObj.GenerateRandomStars();
  }

  // ***************************
  // Grid of stars. All have same magnitiude and defualt SED
  // ***************************
  if( pCGParams->makeStarGrid() ) {
	StarObject starGridObj(pCGParams,pCGParams->fCatFile);
	starGridObj.GenerateGridStars();
  }

  // ************************************************************************
  // Random Galaxies
  // ************************************************************************
  if ( pCGParams->makeGalaxies() ) {
	GalaxyObject galaxyObj(pCGParams,pCGParams->fCatFile);
	galaxyObj.GenerateComplexGalaxies(pCGParams->fGalaxiesDiameterFOVDeg);
  }

  // Display number of objects creates (stars + grid + galaxies) in file.
  std::cout<< "Total Number Simulated objects: " 
		   << pCGParams->fCatalogObjectCount << std::endl; 
 
  return 0;
}			  

