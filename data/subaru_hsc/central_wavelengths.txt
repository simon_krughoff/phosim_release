# approximate data for optimizations for each filter configuration
#
# rows are each filter configuration
# columns are:
# (1): optics file filter configuration number (x in optics_x.txt)
# (2): name
# (3): minimum wavelength of each band (microns)
# (4): maximum wavelength of each band (microns)
# (5): nominal wavelength of each band (microns)
# (6): approximate plate scale (microns per degree)
#
#(1)(2)	(3)   (4)   (5)   (6)
0   g	0.30  1.20  0.48  317647.0
1   r	0.30  1.20  0.62  317647.0
2   i	0.30  1.20  0.75  317647.0
3   z	0.30  1.20  0.87  317647.0
4   Y	0.30  1.20  0.97  317647.0

