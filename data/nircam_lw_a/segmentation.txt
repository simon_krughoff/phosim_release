# device segmentation data
# 
# (a): device name
# (b): number of amplifiers
# (c): pixels x
# (d): pixels y
#  
# (1): amplifier name
# (2-5): x low, x high, y low, y high (pixels)
# (6): serialread
# (7): parallelread
# (8-9): gain, % variation (electrons/ADU)
# (10-11): bias level, % variation (V)
# (12-13): readnoise , % variation (electrons)
# (14-15): dark current, % variation (electrons/s)
# (16-19): parallel prescan, serial overscan, serial prescan, parallel overscan (pixel)
# (20): hot pixel rate per readout
# (21): hot column rate per readout
# (22): bits per pixel
# (23-35): crosstalk matrix
# 
# (a)    (b)       (c)          (d)
# (1)    (2)   (3)      (4) (5)   (6)(7)(8 9)     (10 11)    (12 13)   (14 15)     (16-19)           (20)(21)   (22) (23-)
A5	   4	    2048         2048
A5_1     0   2039     0     511   -1 1  1.84 20.0 1000.0 9.0 13.33 1.5 0.0335 9.2  0.0 0.0 0.0 0.0   0.0005 0.0 16   1.0 0.0 0.0 0.0
A5_2     0   2039     512   1023  -1 1  1.84 20.0 1000.0 9.0 13.33 1.5 0.0335 9.2  0.0 0.0 0.0 0.0   0.0008 0.0 16   0.0 1.0 0.0 0.0
A5_3     0   2039     1024  1535  -1 1  1.84 20.0 1000.0 9.0 13.33 1.5 0.0335 9.2  0.0 0.0 0.0 0.0   0.0008 0.0 16   0.0 0.0 1.0 0.0
A5_4     0   2039     1536  2047  -1 1  1.84 20.0 1000.0 9.0 13.33 1.5 0.0335 9.2  0.0 0.0 0.0 0.0   0.0006 0.0 16   0.0 0.0 0.0 1.0