# focal plane geometry layout data
# 
# rows are the chips in the focal plane
# columns are the name, x position (microns), y position (microns),
# pixel size (microns), number of x pixels, number of y pixels,
# device material (silicon or MCT) 
# type of device (note CMOS also means Fast Frame CCD, and CCD with 0 readout time also means CMOS where you can set the exposure),
# readout integration mode (frame or sequence)
# readout time (CCD) or frame rate (CMOS)
# group of sensors, 3 euler rotations (degrees), 3 translations (mm)
# deformation type, deformation coefficients, QE variation (obsolete)
#
Ponyo	    -31507.5     -63420.0   15.0  4225 2048 silicon CCD frame  0.0  200.0  Group0  0.00000 0.00000 0.00000 0.00000 0.00000 0.00000  zern      0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00  0.00  0.00  0.00  0.00  0.00  0.00    0.00 
San	        -31492.5     -32070.0   15.0  4225 2048 silicon CCD frame  0.0  200.0  Group0  0.00000 0.00000 0.00000 0.00000 0.00000 0.00000  zern      0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00  0.00  0.00  0.00  0.00  0.00  0.00    0.00
Satsuki	    -31522.5       -465.0   15.0  4225 2048 silicon CCD frame  0.0  200.0  Group0  0.00000 0.00000 0.00000 0.00000 0.00000 0.00000  zern      0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00  0.00  0.00  0.00  0.00  0.00  0.00    0.00
Sheeta	    -31147.5      31380.0   15.0  4225 2048 silicon CCD frame  0.0  200.0  Group0  0.00000 0.00000 0.00000 0.00000 0.00000 0.00000  zern      0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00  0.00  0.00  0.00  0.00  0.00  0.00    0.00
Sophie	    -30817.5      62655.0   15.0  4225 2048 silicon CCD frame  0.0  200.0  Group0  0.00000 0.00000 0.00000 0.00000 0.00000 0.00000  zern      0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00  0.00  0.00  0.00  0.00  0.00  0.00    0.00
Chihiro	     31312.5     -63780.0   15.0  4225 2048 silicon CCD frame  0.0  200.0  Group0  0.00000 0.00000 0.00000 0.00000 0.00000 0.00000  zern      0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00  0.00  0.00  0.00  0.00  0.00  0.00    0.00
Clarisse     31897.5     -32400.0   15.0  4225 2048 silicon CCD frame  0.0  200.0  Group0  0.00000 0.00000 0.00000 0.00000 0.00000 0.00000  zern      0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00  0.00  0.00  0.00  0.00  0.00  0.00    0.00
Fio	         32287.5       -660.0   15.0  4225 2048 silicon CCD frame  0.0  200.0  Group0  0.00000 0.00000 0.00000 0.00000 0.00000 0.00000  zern      0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00  0.00  0.00  0.00  0.00  0.00  0.00    0.00
Kiki	     32257.5      30840.0   15.0  4225 2048 silicon CCD frame  0.0  200.0  Group0  0.00000 0.00000 0.00000 0.00000 0.00000 0.00000  zern      0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00  0.00  0.00  0.00  0.00  0.00  0.00    0.00
Nausicaa     32242.5      62310.0   15.0  4225 2048 silicon CCD frame  0.0  200.0  Group0  0.00000 0.00000 0.00000 0.00000 0.00000 0.00000  zern      0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00  0.00  0.00  0.00  0.00  0.00  0.00    0.00
