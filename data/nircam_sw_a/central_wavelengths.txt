# approximate data for optimizations for each filter configuration
#
# rows are each filter configuration
# columns are:
# (1): optics file filter configuration number (x in optics_x.txt)
# (2): name
# (3): minimum wavelength of each band (microns)
# (4): maximum wavelength of each band (microns)
# (5): nominal wavelength of each band (microns)
# (6): approximate plate scale (microns per degree)
#
#(1)(2)		(3)   (4)   (5)   (6)
0   F070W	0.30  2.60  0.70  2.10048622366e6
1   F090W	0.30  2.60  0.90  2.10048622366e6
2   F115W	0.30  2.60  1.15  2.10048622366e6
3   F140M	0.30  2.60  1.40  2.10048622366e6
4   F150W	0.30  2.60  1.50  2.10048622366e6
5   F150W2	0.30  2.60  1.50  2.10048622366e6
6   F162M	0.30  2.60  1.62  2.10048622366e6
7   F164N	0.30  2.60  1.64  2.10048622366e6
8   F182M	0.30  2.60  1.82  2.10048622366e6
9   F187N	0.30  2.60  1.87  2.10048622366e6
10  F200W	0.30  2.60  2.00  2.10048622366e6
11  F210M 	0.30  2.60  2.10  2.10048622366e6
12  F212N	0.30  2.60  2.12  2.10048622366e6